#if defined _sm_jail_redie_included
  #endinput
#endif
#define _sm_jail_redie_included

/*********************************************************
 * returns if client is ghost
 *
 * @param client		The client to run the check on
 * @true on match, false if not		
 *********************************************************/
native void IsPlayerGhost(int client);
