
#include <sourcemod>
#include <sdktools>
#include <sdktools_trace>
#include <sdkhooks>
#include <cstrike>
#include <devzones>
#include <smlib>
//#pragma newdecls required
#pragma semicolon 1
#include <sm_jail_redie>
#include <countts>
//#include <warden>

#define PLUGIN_VERSION "v4.0"


Handle esa_cvar;								// для прозрачности sv_disable_immunity_alpha

bool g_dm[MAXPLAYERS+1] = {false, ...};			// находится ли игрок в ДМ
bool noarmas[MAXPLAYERS+1] = {false, ...};		// true - игрок не сможет подбирать оружие
bool cerrado = false;							// отключен ли дм? (например в конце раунда и т.п.)
float g_fOrigin[3];								// положение респа для призраков
//int ghostColor[4] = { 255, 255, 255, 100 };	// цвет и прозрачность призраков
int g_offsCollisionGroup;
int ient;										// спрайт на респавне


int g_iOffset_PlayerResource_Alive = -1;

public Plugin myinfo =
{
	name		= "JAIL ReDie",
	author		= "Franc1sco, ShaRen",
	description	= "",
	version		= PLUGIN_VERSION,
	url			= "Servers-Info.Ru"
}

public void OnPluginStart()
{
	CreateConVar("sm_franugdmminigame_version", PLUGIN_VERSION, "", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	g_offsCollisionGroup = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	RegConsoleCmd("sm_dm", Command_dm);
	RegAdminCmd("sm_respawn_g", Command_Respawn_Ghost, ADMFLAG_ROOT, "Respawns a ghost player." );
	RegConsoleCmd("drop", drop13);
	RegConsoleCmd("sm_mark", CommandMark);
	RegConsoleCmd("sm_dmark", CommandDelMark);
	RegConsoleCmd("sm_dghosts", CommandDelGhosts);
	
	AddNormalSoundHook(Hook_NormalSound);
	HookEvent("round_start", roundStart);
	HookEvent("round_end", Event_Round_End);
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_death", Event_PlayerDeath2,EventHookMode_Pre);
	HookEvent("player_footstep", Event_PlayerFootstep, EventHookMode_Pre);
	g_iOffset_PlayerResource_Alive = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	
	for (int i = 1; i < GetMaxClients(); i++)
		if (IsClientInGame(i)) OnClientPutInServer(i);
	CreateTimer(10.0, Message, _, TIMER_REPEAT);
	
	esa_cvar = FindConVar("sv_disable_immunity_alpha");
	if(esa_cvar == INVALID_HANDLE)
		return;
		
	SetConVarInt(esa_cvar, 1);
	HookConVarChange(esa_cvar, ConVarChanged);
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("IsPlayerGhost", Native_IsPlayerGhost);		//#include <sm_jail_redie>
	return APLRes_Success;
}

/*Если игрок призрак*/
public Native_IsPlayerGhost(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	if(!IsClientInGame(client) && !IsClientConnected(client))
		ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", client);
		
	return (g_dm[client]) ? true:false;
}

public ConVarChanged(Handle:cvar, const String:oldVal[], const String:newVal[])
	SetConVarInt(esa_cvar, 1);

public Action:CommandDelMark(client,	args)
{
	if (IsValidClient(client) && GetClientTeam(client) == 3) {		// && warden_iswarden(client)
		DelMark();
		GhostsKillMenu(client);
	} else PrintToChat(client, "Только КМД может удалять спавн призраков");
}

public	Action CommandDelGhosts(client,	args) {
	GhostsKillMenu(client);
}

public	Action CommandMark(client,	args)
{
	if (IsValidClient(client) && GetClientTeam(client) == 3 && GamesAvailable())// && warden_iswarden(client) 
	{
		//if	(ient > 0 && IsValidEdict(ient))
		//	RemoveEdict(ient);
		//decl Float:g_fAngles[3];
		//GetClientEyePosition(client, g_fOrigin);
		//GetClientEyeAngles(client, g_fAngles);
		//
		//TR_TraceRayFilter(g_fOrigin, g_fAngles, MASK_SOLID, RayType_Infinite, Trace_FilterPlayers, client);
		//if(TR_DidHit(INVALID_HANDLE))
		//{
		//	TR_GetEndPosition(g_fOrigin, INVALID_HANDLE);
		//	TR_GetPlaneNormal(INVALID_HANDLE, g_fAngles);
		//	GetVectorAngles(g_fAngles, g_fAngles);
		//	g_fAngles[0] += 90.0;
		//}
		//g_fOrigin[2] += 20.0;
		////PrintToChat(client, "%.1f %.1f %.1f", g_fOrigin[0], g_fOrigin[1], g_fOrigin[2]);
		//for (int i; i < MAXPLAYERS; i++)
		//	if(IsValidClient(i) && !IsPlayerAlive(i) && !g_dm[i])
		//		DID2(i);
		//ient = CreateEntityByName("env_sprite");
		//if(ient	== -1 ) return Plugin_Continue;
		//DispatchKeyValue(ient, "model", "materials/vgui/logos/spray.vmt");
		//DispatchSpawn(ient);
		//TeleportEntity(ient, g_fOrigin, g_fAngles, NULL_VECTOR);
		PrintToChat(client, "Пока не работает =(");
	} else if (!GamesAvailable()) {
		PrintToChat(client, "Сперва соберите всех Т и проверьте командой !check");
		PrintToChat(client, "Создавайте призраков только чтобы они они играли в игры");
	} else PrintToChat(client, "Только живые КТ может создавать спавн призраков");
	
	return Plugin_Continue;
}

public bool:Trace_FilterPlayers(entity, contentsMask, any:data)
{
	return(entity != data && entity > MaxClients) ? true:false;
}

DelMark()
{
	g_fOrigin = view_as<float>({0.0, 0.0, 0.0});
	if	(ient > 0 && IsValidEdict(ient)) RemoveEdict(ient);
}

public Action:Message(Handle:timer)
	for (int i=1; i<MAXPLAYERS; i++)
		if (IsValidClient(i)  &&  g_dm[i])
			PrintHintText(i, "Ты призрак, чтобы выйти введи !dm в чат");

public OnMapStart()
{
	int entity = FindEntityByClassname(0, "cs_player_manager");
	SDKHook(entity, SDKHook_ThinkPost, OnPlayerManager_ThinkPost);
}

public OnPlayerManager_ThinkPost(entity)			//чтобы игроки в TAB отображались мертвыми
	for (int i=1; i<MAXPLAYERS; i++)
		if(g_dm[i])
			SetEntData(entity, (g_iOffset_PlayerResource_Alive+i*4), 0, 1, true);


public OnClientDisconnect(client)
{
	g_dm[client] = false;
	noarmas[client] = false;
}

public Action drop13(client, args)				// не дает игроку что-либо выкидывать
{
	if(g_dm[client]) return Plugin_Handled;
	return Plugin_Continue;
}

public Action:Command_dm(client, args)
{
	//if (warden_iswarden(client))
	//{
	//	GhostMenu(client)
	//	return Plugin_Continue
	//}
	if (IsValidClient(client) && IsPlayerAlive(client) && !g_dm[client] && GetClientTeam(client) == 3) {
		GhostMenu(client);
		return Plugin_Continue;
	}
	if(g_fOrigin[0] == 0.0) {
		PrintToChat(client, "Отсутствует точка возрождения");
		return Plugin_Continue;
	}
	
	if (!cerrado  &&  !IsPlayerAlive(client)  &&  !g_dm[client]  &&  IsValidClient(client)  &&  GetClientTeam(client) > 1) {
		g_dm[client] = true;
		CreateTimer(0.5, Respawn_ghost, client);
		PrintToChat(client,"Сайчас ты станешь призраком");
	} else if (g_dm[client]) {
		PrintToChat(client,"Теперь ты не призрак");
		if (IsClientInGame(client) && g_dm[client]) {
			if(IsPlayerAlive(client)) {
				ForcePlayerSuicide(client);			// убиваем и сохраняем фраги и смерти
				SetEntProp(client, Prop_Data, "m_iFrags", GetClientFrags(client)+1);
				int olddeaths = GetEntProp(client, Prop_Data, "m_iDeaths");
				SetEntProp(client, Prop_Data, "m_iDeaths", olddeaths-1);
			}
			g_dm[client] = false;
			noarmas[client] = false;
		}
	} else if (IsPlayerAlive(client)) {
		g_dm[client] = false;
		PrintToChat(client, "Оказано, ты ещё живой");
	} else if (cerrado) {
		g_dm[client] = false;
		PrintToChat(client,"Оказано, в этот момент нельзя возродиться");
	} else if (GetClientTeam(client) < 2) {
		g_dm[client] = false;
		PrintToChat(client,"Оказано, войди в команду");
	}
	return Plugin_Handled;
}

public Action Event_PlayerDeath2(Handle:event, const String:name[], bool:dontBroadcast)
{
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	return(g_dm[victim]) ? Plugin_Handled: Plugin_Continue;
}

public Action Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(g_dm[client]) {
		CreateTimer(2.0, Respawn_ghost, client);
		//PrintToChatAll("CreateTimer(2.0, Respawn_ghost, %i)", client);
		return Plugin_Continue;
	} else if(!cerrado && g_fOrigin[0] != 0.0)
		DID2(client);								// меню с возрождалкой

	int iAliveT, iAliveCT;
	Team_GetAliveClientCounts(iAliveT, iAliveCT);	//это измененный <smlib>

	if(iAliveT == 0 || iAliveCT == 0)
		GhostsDelete();
	return Plugin_Continue;
}

public GhostsDelete()									// выгоняем всех из ДМ
{
	cerrado = true;
	for (int i=1; i<MAXPLAYERS; i++)
		if (IsClientInGame(i) && g_dm[i]) {
			if(IsPlayerAlive(i)) {
				ForcePlayerSuicide(i);											// убиваем и сохраняем фраги/смерти ...
				SetEntProp(i, Prop_Data, "m_iFrags", GetClientFrags(i)+1);		// ... чтобы был конец раунда
				int olddeaths = GetEntProp(i, Prop_Data, "m_iDeaths");
				SetEntProp(i, Prop_Data, "m_iDeaths", olddeaths-1);
			}
			g_dm[i] = false;
			noarmas[i] = false;
		}
}

public Action GhostsKillMenu(clientId) 
{
	Menu menu3 = CreateMenu(GhostsKillMenuHandler2);
	menu3.SetTitle("JAIL ghost\nУдалить всех призраков?");
	menu3.AddItem("opcion1", "ДА");
	menu3.AddItem("opcion2", "НЕТ");
	menu3.ExitButton = true;
	menu3.Display(clientId, 15);
	return Plugin_Handled;
}

public GhostsKillMenuHandler2(Menu menu, MenuAction:action, client, itemNum) 
{
	if ( action == MenuAction_Select ) {
		GhostsDelete();
		DelMark();
	} else if (action == MenuAction_End)
		delete menu;
}

public Action:GhostMenu(clientId) 
{
	Menu menu2 = CreateMenu(GhostsMenuHandler2);
	menu2.SetTitle("JAIL ghost");
	menu2.AddItem("opcion11", "Создать точку возрождения");
	menu2.AddItem("opcion22", "Удалить точку возрождения");
	menu2.AddItem("opcion33", "Удалить всех призраков");
	menu2.ExitButton = true;
	menu2.Display(clientId, 15);
	return Plugin_Handled;
}

public GhostsMenuHandler2(Menu menu, MenuAction:action, client, itemNum) 
{
	if ( action == MenuAction_Select && IsValidClient(client) && IsPlayerAlive(client) && !g_dm[client] && GetClientTeam(client) == 3) {
		char info[32];
		menu.GetItem(itemNum, info, sizeof(info));
		if (strcmp(info,"opcion11") == 0 && !cerrado) {
			FakeClientCommand(client, "sm_mark");
			PrintToChatAll("%N создал спавн прзраков", client);
		}
		if (strcmp(info,"opcion22") == 0 && !cerrado)
			DelMark();
		if (strcmp(info,"opcion33") == 0 && !cerrado) {
			GhostsDelete();
			DelMark();
			cerrado = false;
		}
	} else if (action == MenuAction_End)
		delete menu;
}

public Action DID2(clientId) 
{
	Menu menu2 = CreateMenu(DIDMenuHandler2);
	menu2.SetTitle("JAIL ghost\nВы хотите стать призраком?");
	menu2.AddItem("opcion1", "ДА");
	menu2.AddItem("opcion2", "НЕТ");
	menu2.ExitButton = true;
	menu2.Display(clientId, 15);
	return Plugin_Handled;
}

public DIDMenuHandler2(Menu menu, MenuAction:action, client, itemNum) 
{
	if ( action == MenuAction_Select ) {
		char info[32];
		menu.GetItem(itemNum, info, sizeof(info));
		if (strcmp(info,"opcion1") == 0 && !cerrado && !IsPlayerAlive(client)) {
			g_dm[client] = true;
			CreateTimer(0.5, Respawn_ghost, client);
		}
	} else if (action == MenuAction_End)
		delete menu;
}

public Action:Event_Round_End(Handle:event, const String:name[], bool:dontBroadcast)
{
	DelMark();
	GhostsDelete();
}

public Action Command_Respawn_Ghost(client, args)
{
	if(!client) return Plugin_Handled;
	if(args < 1) {
		ReplyToCommand( client, "[SM] Usage: sm_respawn_g <player>" );
		return Plugin_Handled;
	}
	decl String:name[64];
	GetCmdArg( 1, name, sizeof(name) );
	int target = FindTarget( client, name );
	if( target == -1 ) return Plugin_Handled;
	if( !IsClientInGame(target) ) {
		ReplyToCommand( client, "[SM] sm_respawn_g: Invalid target." );
		return Plugin_Handled;
	}
	if( GetClientTeam(target) < 2 || IsPlayerAlive(target) ) {
		ReplyToCommand( client, "[SM] sm_respawn_g: Invalid target." );
		return Plugin_Handled;
	}
	g_dm[target] = true;
	CreateTimer(0.01, Respawn_ghost, target);
	
	if( !IsPlayerAlive(target) )
		ReplyToCommand( client, "[SM] Respawned %N.", target );
		
	PrintToChat( target, "[SM] An admin respawned you as ghost." );
	
	return Plugin_Handled;
}

public Action Respawn_ghost(Handle timer, any:client)
{
	if (g_fOrigin[0] == 0.0 && g_fOrigin[1] == 0.0 && g_fOrigin[2] == 0.0) {
		g_dm[client] = false;
		PrintToChat(client, "Нет точки возрождения");
	} else if(IsValidClient(client) && GetClientTeam(client) > 1 && g_dm[client] && !cerrado && !IsPlayerAlive(client)) {
		CS_RespawnPlayer(client);
		int weaponIndex;
		for (int i = 0; i <= 3; i++)
			if ((weaponIndex = GetPlayerWeaponSlot(client, i)) != -1) {
				RemovePlayerItem(client, weaponIndex);
				RemoveEdict(weaponIndex);
			}
		SetEntData(client, g_offsCollisionGroup, 2, 4, true);		// делаем no-block
		//SetEntProp(client, Prop_Send, "m_lifeState", 0);			// делаем игрока типа мертвым 0 или 1
		SetEntProp(client, Prop_Data, "m_ArmorValue", 0);
		SetEntProp(client, Prop_Send, "m_bHasDefuser", 0);
		noarmas[client] = false;										// чтобы в этот момент мог получить оружие
		if (GetClientTeam(client) == 3) GivePlayerItem(client, "weapon_knife");
		else GivePlayerItem(client, "weapon_knife_t");
		TeleportEntity(client, g_fOrigin, NULL_VECTOR, NULL_VECTOR);
		SetEntityRenderMode(client, RENDER_TRANSCOLOR);
		SetEntityRenderColor(client, 255, 255, 255, 180);
		noarmas[client] = true;
	} else if (IsPlayerAlive(client)) {
		g_dm[client] = false;
		PrintToChatAll("%N должен быть мертв", client);
	} else if (GetClientTeam(client) < 2 ) {
		g_dm[client] = false;
		PrintToChatAll("%N должен быть в команде", client);
	} else if (!g_dm[client])
		PrintToChatAll("%N не призрак", client);
	else if (cerrado) {
		g_dm[client] = false;
		PrintToChatAll("В данный момент возрождаться нельзя");
	}
}

public Action roundStart(Handle:event, const String:name[], bool:dontBroadcast) 
{
	DelMark();
	cerrado = false;
	for (int i=1; i<MAXPLAYERS; i++)
		if (IsClientInGame(i)) {
			g_dm[i] = false;
			noarmas[i] = false;
		}
}

public IsValidClient( client )
{
	return (0<client<=MaxClients && IsClientInGame(client)) ? true:false;
}

public Zone_OnClientLeave(client, String:zone[])
{
	if(StrContains(zone, "dmzone", false) == 0 && IsValidClient(client) && g_dm[client] && !cerrado) {
		PrintToChat(client, "\x03Dont go out of DeathMach!");
		decl Float:Position[3];
		if(GetClientTeam(client) == CS_TEAM_CT) {
			if(Zone_GetZonePosition("dm2", false, Position))
				TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
		} else if(GetClientTeam(client) == CS_TEAM_T)
			if(Zone_GetZonePosition("dm1", false, Position))
				TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
	}
}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKHook(client, SDKHook_WeaponDropPost, OnWeaponDrop);
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	//SDKHook(client, SDKHook_SetTransmit, Hook_SetTransmit);
}

public Action OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if(!IsValidClient(attacker)) return Plugin_Continue;
	if(g_dm[attacker]) return Plugin_Handled;			// не дает призракам атаковать кого-либо
	return Plugin_Continue;
}

public OnWeaponDrop(client, entity)
{
	if (!IsClientInGame(client) || !IsValidEdict(entity) || GetClientHealth(client) > 0 || !g_dm[client])
		return;
	AcceptEntityInput(entity, "kill");
}

public Action OnWeaponCanUse(client, weapon)
{
	return (noarmas[client]) ? Plugin_Handled:Plugin_Continue;
}

//public Action:Hook_SetTransmit(entity, client) 
//{
//	if (entity != client && g_dm[client] != g_dm[entity] && IsPlayerAlive(client))
//		return Plugin_Handled
//	
//	return Plugin_Continue
//}

public Action Hook_NormalSound(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{
	// Ignore non-weapon sounds.
	if (!(strncmp(sample, "weapons", 7) == 0 || strncmp(sample[1], "weapons", 7) == 0))
		return Plugin_Continue;
		
		
	//PrintHintTextToAll("es %i",entity);
	int client = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if(!IsValidClient(client) || !g_dm[client])
		return Plugin_Continue;
	
	for (int i=0; i<numClients; i++)
		if (!g_dm[clients[i]]) {
			// Remove the client from the array.
			for (int j=i; j<numClients-1; j++)
				clients[j] = clients[j+1];
			
			numClients--;
			i--;
		}
	return (numClients > 0) ? Plugin_Changed : Plugin_Stop;
}

public Action Event_PlayerFootstep(Event e, const String:name[], bool:dontBroadcast)
{
	return g_dm[GetClientOfUserId(e.GetInt("userid"))] ? Plugin_Handled : Plugin_Continue;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(g_dm[client])
		buttons &= ~IN_USE;
	
	return Plugin_Changed;
}